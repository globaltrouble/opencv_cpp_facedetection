#ifndef MAIN_H
#define MAIN_H

#include <vector>
#include <string>

#include <boost/filesystem/path.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <opencv2/core.hpp>

namespace fs = boost::filesystem;
namespace pt = boost::property_tree;


struct Arguments
{
    std::string file_extension;
    std::string path_to_source_dir;
    fs::path path_to_result_dir;
    std::string path_to_model_config;
    std::string path_to_model_weights;
    bool verbose = 1;
};


Arguments parse_arguments(int argc, char** argv);

void write_results(const Arguments& args, const pt::ptree& result);

void add_to_results(pt::ptree& result,
                    std::vector<cv::Rect>& faces,
                    std::string& fpath);

#endif // MAIN_H
