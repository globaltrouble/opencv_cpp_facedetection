#include <iostream>
#include <utility>

#include <boost/filesystem.hpp>

#include <boost/program_options.hpp>

#include "main.h"
#include "../lib/facedetector.h"

namespace fd = facedetector;
namespace po = boost::program_options;


int main(int argc, char *argv[])
{
    const Arguments args = parse_arguments(argc, argv);
    std::vector<std::string> files;
    try
    {
        files = fd::find_images(args.path_to_source_dir, args.file_extension);
    }
    catch (std::invalid_argument& e)
    {
        std::cerr << "Can't parse dir: `" << args.path_to_source_dir << "`, reason: `" << e.what() << "`" << std::endl;
        exit(3);
    }

    fd::init_detector(args.path_to_model_config, args.path_to_model_weights);
    pt::ptree result;
    for (std::string fpath : files)
    {
        cv::Mat img;
        std::vector<cv::Rect> faces;
        try
        {
            img = fd::load_image(fpath);
        }
        catch (std::invalid_argument& e)
        {
            std::cerr << "Can't load image: `" << fpath << "`, reason: `" << e.what() << "`" << std::endl;
            continue;
        }

        try
        {
            faces = fd::get_bounding_boxes(img);
        }
        catch (cv::Exception& e)
        {
            std::cerr << "Can't process image: `" << fpath << "`, reason: `" << e.what() << "`" << std::endl;
            continue;
        }

        if (args.verbose)
            std::cout << "Found " << faces.size() << ", img " << fpath << std::endl;

        if (faces.empty()) continue;

        fd::BoundingBoxParams params;
        params.blur = true;
        fd::add_bounding_boxes(faces, img, params);

        double rescale = 0.5;
        fs::path dst_path = (args.path_to_result_dir / fs::path(fpath).filename());
        fd::save_image(img, dst_path.string(), rescale);

        add_to_results(result, faces, fpath);
    }

    write_results(args, result);
    return 0;
}


Arguments parse_arguments(int argc, char** argv)
{
    Arguments args;

    po::options_description desc("Detect faces on images");
    desc.add_options()
    ("source,s",   po::value<std::string>(&args.path_to_source_dir)->required(),      "Path to source dir with images")
    ("extension,e",   po::value<std::string>(&args.file_extension)->required(),      "Target file extension, example `jpeg`")
    ("result,r",   po::value<fs::path>(&args.path_to_result_dir)->required(),      "Path, where to store result file `result.json`")
    ("verbose,v", "Show additional logs")

    ("config,c",  po::value<std::string>(&args.path_to_model_config)->required(),   "Path to model config, wich will be used for detection")
    ("weights,w",  po::value<std::string>(&args.path_to_model_weights)->required(),   "Path to model weights");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    return args;
}


void add_to_results(pt::ptree& result,
                    std::vector<cv::Rect>& faces,
                    std::string& fpath)
{
    pt::ptree faces_from_img;
    for (cv::Rect r : faces)
    {
        pt::ptree face_descr;

        face_descr.put("x", r.x);
        face_descr.put("y", r.y);
        face_descr.put("width", r.width);
        face_descr.put("height", r.height);

        faces_from_img.push_back(std::make_pair("", face_descr));
    }

    result.add_child(pt::ptree::path_type(fs::path(fpath).string(), '\0'), faces_from_img);
}


void write_results(const Arguments& args, const pt::ptree& result)
{
    if (!fs::exists(args.path_to_result_dir))
        fs::create_directories(args.path_to_result_dir);

    const fs::path path_to_result_file = fs::path(args.path_to_result_dir) / fs::path("result.json");
    pt::write_json(path_to_result_file.string(), result);
}

