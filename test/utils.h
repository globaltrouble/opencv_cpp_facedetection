#ifndef UTILS_H
#define UTILS_H

#include <opencv2/core.hpp>


namespace utils
{
    bool is_equal(const cv::Mat &a, const cv::Mat &b);
}


#endif // UTILS_H
