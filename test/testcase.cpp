#include <iostream>

#include <boost/filesystem/path.hpp>
#include <boost/filesystem.hpp>

#include <gtest/gtest.h>

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>

#include "../lib/facedetector.h"
#include "utils.h"

using namespace std;
using namespace facedetector;

namespace fs = boost::filesystem;


const string no_faces_rel_path = "files/no_faces.jpeg";
const string singel_face_rel_path = "files/single_face_from_dne_gan.jpeg";
const string multiple_faces_rel_path = "files/multiple_faces_from_wider_dataset.jpeg";
const string model_config_rel_path = "files/yolov4-leaky-416-s32.cfg";
const string model_weights_rel_path = "files/yolov4-leaky-416-s32-colab-1000.weights";


class TestFindImages : public ::testing::Test
{

public:
    const fs::path temp_target_dir = fs::absolute("416de2b8-7308-42e8-9bb1-a3194599505c");
    const fs::path temp_inclded_dir = fs::absolute(temp_target_dir / "1612d754-aa53-4da5-8ce1-94fc1a3f59a2");

    const string format = "jpg";

    const vector<fs::path> target_files =
    {
        temp_target_dir / "1.jpg",
        temp_target_dir / "2.jpg",
        temp_inclded_dir / "3.jpg"
    };

    const vector<fs::path> nontarget_files =
    {
        temp_target_dir / "4",
        temp_target_dir / "5.jpgg",
        temp_inclded_dir / "6.jpg.jjpg",
        temp_inclded_dir / "7.jpeg"

    };

    void SetUp() override
    {
        fs::remove_all(temp_target_dir);
        fs::create_directories(temp_inclded_dir);
    }

    void TearDown() override
    {
        fs::remove_all(temp_target_dir);
    }

    void create_temp_files()
    {
        for (auto & fpath : target_files)
        {
            fs::ofstream(fpath.c_str());
        }

        for (auto & fpath : nontarget_files)
        {
            fs::ofstream(fpath.c_str());
        }
    };
};


TEST_F(TestFindImages, test_find_images)
{
    string unexisting_dir("e588dd82-5623-4c50-847e-0ef75880fc3f");
    ASSERT_THROW(find_images(unexisting_dir, format), std::invalid_argument);

    vector<string> results = find_images(temp_target_dir.string(), format);
    ASSERT_EQ(results.size(), 0);

    create_temp_files();

    results = find_images(temp_target_dir.string(), format);
    ASSERT_EQ(results.size(), target_files.size());

    for (auto & fpath : target_files)
    {
        auto p = find(results.begin(), results.end(), fpath.string());
        ASSERT_NE(p, results.end());
    }

    results = find_images(temp_inclded_dir.string(), format);
    ASSERT_EQ(results.size(), 1);

    for (auto & fpath : nontarget_files)
    {
        auto p = find(results.begin(), results.end(), fpath.string());
        ASSERT_EQ(p, results.end());
    }
}


TEST(TestLoadFile, test_load_file)
{
    const int example_width = 1024;
    const int example_height = 1024;

    fs::path example = fs::absolute(singel_face_rel_path);
    cv::Mat img = load_image(example.string());
    cv::Size size = img.size();

    ASSERT_EQ(size.width, example_width);
    ASSERT_EQ(size.height, example_height);

    fs::path unexisting = fs::absolute(singel_face_rel_path + "unexising");
    ASSERT_THROW(load_image(unexisting.string()), std::invalid_argument);

    fs::path dir = example.parent_path();
    ASSERT_THROW(load_image(dir.string()), std::invalid_argument);
}


TEST(TestGetBoundingBoxes, test_get_bounding_boxes)
{
    const string model_config = fs::absolute(model_config_rel_path).string();
    const string model_weights = fs::absolute(model_weights_rel_path).string();

    const string example_not_face = fs::absolute(no_faces_rel_path).string();
    cv::Mat img = load_image(example_not_face);

    init_detector(model_config, model_weights);
    auto results = get_bounding_boxes(img);
    ASSERT_EQ(results.size(), 0);

    auto check_results_bounds = [](
                                    const vector<cv::Rect> &areas,
                                    const cv::Mat &img
                                )
    {
        cv::Size img_size = img.size();

        for (auto & r : areas)
        {
            ASSERT_LE(r.height + r.y, img_size.height);
            ASSERT_LE(r.width + r.x, img_size.width);
        }
    };

    const fs::path example_single_face = fs::absolute(singel_face_rel_path);
    img = load_image(example_single_face.string());

    results = get_bounding_boxes(img);
    ASSERT_EQ(results.size(), 1);
    check_results_bounds(results, img);

    const fs::path example_multiple_face = fs::absolute(multiple_faces_rel_path);
    img = load_image(example_multiple_face.string());

    results = get_bounding_boxes(img);
    ASSERT_EQ(results.size(), 2);
    check_results_bounds(results, img);
}


TEST(TestAddBoundingBoxes, test_add_bounding_boxes)
{
    cv::Mat original = {640, 480, CV_8UC3, cv::Scalar(0, 0, 0)};
    cv::randu(original, cv::Scalar(0, 0, 0), cv::Scalar(255, 255, 255));

    cv::Mat with_boxes;
    original.copyTo(with_boxes);

    cv::Rect roi = {20, 10, 30, 40};

    ASSERT_TRUE(utils::is_equal(with_boxes, original));

    BoundingBoxParams params;
    params.thikness = 1;
    params.color = cv::Scalar(255, 255, 0);
    params.blur = false;

    cv::Rect box_roi =
    {
        roi.x - params.thikness,
        roi.y - params.thikness,
        roi.width + params.thikness * 2,
        roi.height + params.thikness * 2
    };
    vector<cv::Rect> boxes = {box_roi};
    add_bounding_boxes(boxes, with_boxes, params);
    ASSERT_FALSE(utils::is_equal(with_boxes, original));

    ASSERT_TRUE(utils::is_equal(with_boxes(roi), original(roi)));

    cv::Rect lower_roi =
    {
        0, 0, original.size().width, box_roi.y
    };
    ASSERT_TRUE(utils::is_equal(with_boxes(lower_roi), original(lower_roi)));

    cv::Rect upper_roi =
    {
        0,
        box_roi.y + box_roi.height,
        original.size().width,
        original.size().height - box_roi.y - box_roi.height
    };
    ASSERT_TRUE(utils::is_equal(with_boxes(upper_roi), original(upper_roi)));


    original.copyTo(with_boxes);
    ASSERT_TRUE(utils::is_equal(with_boxes, original));

    params.blur = true;
    add_bounding_boxes(boxes, with_boxes, params);

    ASSERT_FALSE(utils::is_equal(with_boxes, original));
    ASSERT_FALSE(utils::is_equal(with_boxes(roi), original(roi))); // wil be blured

    ASSERT_TRUE(utils::is_equal(with_boxes(lower_roi), original(lower_roi)));
    ASSERT_TRUE(utils::is_equal(with_boxes(upper_roi), original(upper_roi)));
}


class TestSaveImage : public ::testing::Test
{

public:
    const fs::path temp_dir = fs::absolute("8be1d2ce-827a-4549-8c73-04de6260ff31");

    void SetUp() override
    {
        fs::remove_all(temp_dir);
    }

    void TearDown() override
    {
        fs::remove_all(temp_dir);
    }
};

TEST_F(TestSaveImage, test_save_image)
{
    string fname = "new_img.jpg";
    string target_path = (temp_dir / fname).string();

    ASSERT_FALSE(fs::exists(target_path));

    cv::Mat original = {320, 240, CV_8UC3, cv::Scalar(0, 0, 0)};
    cv::randu(original, cv::Scalar(0, 0, 0), cv::Scalar(255, 255, 255));

    double rescale = 1.0;
    save_image(original, (temp_dir / fname).string(), rescale);
    ASSERT_TRUE(fs::exists(target_path));

    cv::Mat loaded = load_image(target_path);

    cv::Size loaded_size = loaded.size();
    cv::Size orig_size = original.size();

    ASSERT_EQ(loaded_size.width, orig_size.width);
    ASSERT_EQ(loaded_size.height, orig_size.height);

    rescale = 0.5;
    save_image(original, (temp_dir / fname).string(), rescale);
    loaded = load_image(target_path);

    loaded_size = loaded.size();
    ASSERT_EQ(loaded_size.width, orig_size.width / 2);
    ASSERT_EQ(loaded_size.height, orig_size.height / 2);
}
