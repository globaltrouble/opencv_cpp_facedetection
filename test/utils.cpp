#include "utils.h"

using namespace std;


namespace utils
{
bool is_equal(const cv::Mat &a, const cv::Mat &b)
{
    cv::Scalar res = cv::sum(a - b);
    int elements_count = sizeof(res.val) / sizeof(double);

    for (int i = 0; i < elements_count; i++)
    {
        if (res.val[i] != 0)
        {
            return false;
        }
    }
    return true;
};
}



