#### create build dir

mkdir build && cd build

#### configure project

cmake --configure ..

#### build project

cmake --build .

#### test build

ctest -C Debug -V

#### install build

checkinstall --pkgname opencv-test-detection 


#### by default model with weights will be copied to `/usr/local/share/facedetector/`

#### usage `detectfaces -v -s PATH_TO_IMAGES_DIR -e EXTENSION -r PATH_TO_SAVE_RESULTS_DIR  -c PATH_TO_MODEL_CONFIG -w PATH_TO_MODEL_WEIGHTS`


#### example run:

detectfaces -v -s ../test/examples -e jpeg -r results  -c ../lib/yolov4-leaky-416-s32.cfg -w ../lib/yolov4-leaky-416-s32-colab-1000.weights


#### explore reults:

echo '============results.json============' &&  cat results/result.json && echo '============rescaled images============' &&  ls -lah results



#### Known issues

##### 1) Gtest didn't compile on windows



#### * Yolo-v4-leaky-416 trained in 1000 epochs on [WIDER faces dataset](http://shuoyang1213.me/WIDERFACE/).


##### Girls
![Girls](files/photo5420636267202326545.jpg)

##### Ladies
![Ladies](files/photo5420636267202326548.jpg)

#### * I've also trained yolo-v5-s and yolo-v5-x using Pytorch (transfer learning, layers 0-9 are frozen and layers 10-24 trained on WIDER faces dataset), but unfortunately it can't be loaded using OpenCV module onnx or dnn. Yolo-v5 model also can be ported to C++ using tensorflow.
