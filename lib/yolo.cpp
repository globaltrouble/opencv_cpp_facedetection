#include "yolo.h"

#include <iostream>

namespace  yolo
{
    std::vector<std::string> get_output_layers_names(const cv::dnn::Net* net)
    {
        std::vector<std::string> names;

        std::vector<int> out_layers = net->getUnconnectedOutLayers();
        std::vector<std::string> layers_names = net->getLayerNames();

        names.resize(out_layers.size());

        for (size_t i = 0; i < out_layers.size(); ++i)
        {
            names[i] = layers_names[out_layers[i] - 1];
        }

        return names;
    }

    template<typename output_data_type>
    std::vector<cv::Rect> filter_output(const cv::Mat &frame,
                                        const std::vector<cv::Mat> &out_layers_predictions)
    {
        // Remove the bounding boxes with low confidence using non-maxima suppression
        double non_max_suppression = 0.5;
        double conf_threshold = 0.5;

        std::vector<int> class_ids;
        std::vector<float> confidences;
        std::vector<cv::Rect> boxes;

        for (auto &layer_prediction : out_layers_predictions)
        {
            // Each Layer = 2dArray([targets X estimation])
            // all layers have different size of targets, due to different grid size,
            // but constant size of estimation, dut to constant settings during training.

            // Targets = flatten([grid_x_size X grid_y_size X bounding_boxes])
            // tragets example = flatten([13 X 13 X 3]) = vector of size 507
            // for each target we estimates class it's position and area.

            // estimation = [x_center, y_center, width, height, prob_target_contains_class, prob_class_1, prob_class_2, ... prob_class_n]
            // size of esitmation  = 5 + number of classes
            // estimation example for 4 classes = [0.2, 0.3, 0.25, 0.11, 0.95, 0.1, 0.3, 0.5, 0.1]

            // Example, layer detects classes on [13 X 13] grid with 3 bounding boxes, for 4 classes
            // targets = vector of size 507 (13 * 13 * 3)
            // estimation = vector of size 9 [0.2, 0.3, 0.25, 0.11, 0.95, 0.1, 0.3, 0.5, 0.1]
            // layer is 2d array [507 X 9]

            // Scan through all the bounding boxes output from the network and keep only the
            // ones with high confidence scores. Assign the box's class label as the class
            // with the highest score for the box.
            for (int j = 0; j < layer_prediction.rows; ++j)
            {
                cv::Mat target = layer_prediction.row(j);
                auto confidence = target.at<output_data_type>(4);

                if (confidence < conf_threshold) continue;

                cv::Mat scores = target.colRange(5, layer_prediction.cols);
                cv::Point class_id_point;

                double class_confidence;
                cv::minMaxLoc(scores, 0, &class_confidence, 0, &class_id_point);

                output_data_type center_x = target.at<output_data_type>(0);
                output_data_type center_y = target.at<output_data_type>(1);
                output_data_type width = target.at<output_data_type>(2);
                output_data_type height = target.at<output_data_type>(3);

                output_data_type left = (center_x - width / 2);
                output_data_type top = (center_y - height / 2);

                auto pair = yolo::normalize_position(left, width, static_cast<output_data_type>(0), static_cast<output_data_type>(1));
                left = pair.first;
                width = pair.second;

                pair = yolo::normalize_position(top, height, static_cast<output_data_type>(0), static_cast<output_data_type>(1));
                top = pair.first;
                height = pair.second;

                class_ids.push_back(class_id_point.x);
                confidences.push_back(class_confidence);
                boxes.push_back(
                    cv::Rect(
                        static_cast<int>(left * frame.cols),
                        static_cast<int>(top * frame.rows),
                        static_cast<int>(width * frame.cols),
                        static_cast<int>(height * frame.rows)
                    )
                );
            }
        }

        // Perform non maximum suppression to eliminate redundant overlapping boxes with lower confidences
        std::vector<int> indices;
        cv::dnn::NMSBoxes(boxes, confidences, conf_threshold, non_max_suppression, indices);

        std::vector<cv::Rect> res;
        for (auto ind : indices)
        {
            res.push_back(boxes[ind]);
        }

        return res;
    }

    template std::vector<cv::Rect> filter_output<float>(const cv::Mat &frame, const std::vector<cv::Mat> &out_layers_predictions);
    template std::vector<cv::Rect> filter_output<double>(const cv::Mat &frame, const std::vector<cv::Mat> &out_layers_predictions);
}
