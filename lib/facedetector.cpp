#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>

#include <opencv2/objdetect.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/dnn.hpp>

#include <boost/filesystem/path.hpp>
#include <boost/filesystem.hpp>

#include "facedetector.h"
#include "yolo.h"


namespace fs = boost::filesystem;


namespace facedetector
{
    static bool was_init = false;
    static std::unique_ptr<cv::dnn::Net> net;
    static std::vector<std::string> layer_names;

    void init_detector(const std::string &model_configuration,
                       const std::string &model_weights)
    {
        if (was_init) return;

        net = std::unique_ptr<cv::dnn::Net>(new cv::dnn::Net(cv::dnn::readNetFromDarknet(model_configuration, model_weights)));
        net->setPreferableBackend(cv::dnn::DNN_BACKEND_OPENCV);
        net->setPreferableTarget(cv::dnn::DNN_TARGET_CPU);

        layer_names = yolo::get_output_layers_names(net.get());

        was_init = true;
    }

    std::vector<std::string> find_images(const std::string &path_to_source_dir,
                                         const std::string &file_extension)
    {
        if (!fs::exists(path_to_source_dir))
            throw std::invalid_argument("Path doesn't exists `" + path_to_source_dir + "`");
        else if (!fs::is_directory(path_to_source_dir))
            throw std::invalid_argument("Is not a dir `" + path_to_source_dir + "`");

        std::vector<std::string> files;
        cv::utils::fs::glob(path_to_source_dir, std::string("*.") + file_extension, files, true);

        return files;
    }

    cv::Mat load_image(const std::string &path)
    {
        if (!fs::exists(path))
            throw std::invalid_argument("Path doesn't exists `" + path + "`");

        else if (!fs::is_regular_file(path))
            throw std::invalid_argument("Is not a file `" + path + "`");

        return cv::imread(path, cv::IMREAD_COLOR);
    }

    std::vector<cv::Rect> get_bounding_boxes(const cv::Mat &img)
    {
        if (!was_init)
            throw std::runtime_error("Yolo model called before initialization");

        double rescale = 1/255.0;
        cv::Size size(416, 416);
        cv::Scalar mean(0,0,0);
        bool swap_rb = true;
        bool crop = false;

        // more information about types can be found here `https://gist.github.com/yangcha/38f2fa630e223a8546f9b48ebbb3e61a`
        int type = CV_32F;
        cv::Mat blob = cv::dnn::blobFromImage(img, rescale, size, mean, swap_rb, crop, type);
        net->setInput(blob);

        std::vector<cv::Mat> cnn_output;
        net->forward(cnn_output, layer_names);

        std::vector<cv::Rect> results = yolo::filter_output<float>(img, cnn_output);
        return results;

    }


    void add_bounding_boxes(const std::vector<cv::Rect> &regions,
                            cv::Mat &img,
                            const BoundingBoxParams& params)
    {
        for (cv::Rect r : regions)
        {
            if (params.blur)
            {
                cv::Mat roi = img(r);
                cv::Mat blured;
                cv::blur(roi, blured, cv::Size(32, 32));
                blured.copyTo(roi);
            }

            rectangle(img,
                    cv::Point(cvRound(r.x),
                              cvRound(r.y)),
                    cv::Point(cvRound(r.x + r.width - 1),
                              cvRound(r.y + r.height - 1)),
                    params.color,
                    params.thikness,
                    params.linetype,
                    params.shift);
        }
    }


    void save_image(const cv::Mat &img,
                    const std::string &fpath,
                    double rescale)
    {
        const fs::path target_path(fpath);

        if (!fs::exists(target_path.parent_path()))
            fs::create_directories(target_path.parent_path());

        cv::Mat resized;
        cv::resize(img, resized, cv::Size(), rescale, rescale);
        cv::imwrite(target_path.string(), resized);
    }
}
