#ifndef YOLO_H
#define YOLO_H

#include <vector>

#include <opencv2/dnn.hpp>


namespace yolo
{
    std::vector<std::string> get_output_layers_names(const cv::dnn::Net* net);

    template<typename output_data_type>
    std::vector<cv::Rect> filter_output(
        const cv::Mat &frame,
        const std::vector<cv::Mat> &outs
    );

    template<typename T>
    inline T clamp(T val, T lower, T upper)
    {
        return std::min(std::max(val, lower), upper);
    }

    template<typename T>
    inline std::pair<T, T> normalize_position(T position, T offset, T min_val, T max_val)
    {
        position = clamp(position, min_val, max_val);
        offset = clamp(offset, min_val, max_val);

        T border = position + offset;
        if (border > max_val)
        {
            double rescale = static_cast<double>(max_val) / static_cast<double>(border);
            position = static_cast<T>(rescale * static_cast<double>(position));
            offset = static_cast<T>(rescale * static_cast<double>(offset));
        }

        return std::make_pair<T, T>(std::move(position), std::move(offset));
    }
}

#endif // YOLO_H
