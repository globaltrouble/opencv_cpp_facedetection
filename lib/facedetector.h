#ifndef FACEDETECTOR_H
#define FACEDETECTOR_H

#include <iostream>
#include <vector>

#include <opencv2/core/cvdef.h> //for CV_EXPORT
#include <opencv2/core/cvstd.hpp> //for cv::String
#include <opencv2/core/utils/filesystem.hpp> //glob
#include <opencv2/core/mat.hpp> // cv::Mat
#include <opencv2/core.hpp>

#include "opencv_test_global.h"


namespace facedetector
{

    struct BoundingBoxParams
    {
        cv::Scalar color = cv::Scalar(255,0,0);
        int thikness = 3;
        int linetype = 8;
        int shift = 0;
        bool blur = false;
    };


    void OPENCV_TEST_EXPORT
    init_detector(const std::string &model_configuration,
                  const std::string &model_weights);

    std::vector<std::string> OPENCV_TEST_EXPORT
    find_images(const std::string &path_to_source_dir,
                const std::string &file_extension);

    cv::Mat OPENCV_TEST_EXPORT
    load_image(const std::string &);

    std::vector<cv::Rect> OPENCV_TEST_EXPORT
    get_bounding_boxes(const cv::Mat& img);


    void OPENCV_TEST_EXPORT
    add_bounding_boxes(const std::vector<cv::Rect>& regions,
                       cv::Mat& img,
                       const BoundingBoxParams& params = BoundingBoxParams());


    void OPENCV_TEST_EXPORT
    save_image(const cv::Mat& img,
               const std::string& fpath,
               double rescale = 1.0);

}
#endif
